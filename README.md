## **tpcc-course** install repo

### Requirements:
- `docker`
- `docker-compose`
- `bash`

### Mac:
- Download [docker installer](https://hub.docker.com/?overlay=onboarding)
- Open & install

### Linux:
- Run `bash docker_setup.sh`
- If you want to get more information on how this works read [instruction](docker.md)

### Installation:
- Run `bash setup.sh`

### Restart container:
Can be useful if your system is shut down/container disappeared/etc
- Run `bash restart_container.sh`

### Log in into container:
Can be useful to manage project files/commit tasks/etc
- Run `bash login_into_container.sh`

Course files are stored at `/tpcc/`

### Troubleshooting:
- If sth becomes broken - save your uncommitted task progress somewhere in safe place.
- Check logs of the container:
```
docker logs tpcc-image
```
- Try to interpret what's being told (using google/man or sth)
- Run installation step once again (important: save your uncommitted progress before)
- If no result - contact course staff

### Useful notes:
- User `user` is designed for clion integration, don't log in as it manually.
  This user may not have permissions to change/use `/tpcc/` dir and is not supposed to do that. Please log in as current user using Log in section.
  
### Clion integration:
- Make sure container is running:
  ```
  docker container list | grep tpcc
  ```
- In Clion:
  1. Setup remote host:
  ```
  username: user
  password: password
  ```
  ![Setup remote host](images/remote-host-setup.png)
  ![Insert credentials](images/credentials.png)
  2. Choose `Remote host` toolchain:
  ![Setup toolchain](images/toolchain.png)
- To use debugger inside tests/code change the following:
  1. Change parameter `FORK_TESTS` in `library/twist/twist/CMakeLists.txt` from 1 to 0
  2. PROFIT
