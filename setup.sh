#!/usr/bin/env bash

if [[ $EUID == 0 ]]; then
   echo "This script must be run as non-root user inside docker group"
   exit 1
fi

mkdir ~/tpcc

git clone https://gitlab.com/Lipovsky/tpcc-course-2019.git ~/tpcc/tpcc

export CONT_UID=$(id -u)
export CONT_GID=$(id -g)

docker-compose -f docker-compose.yml up -d --build --force-recreate
docker exec -it tpcc-image groupadd -g $(id -g) grp
docker exec -it tpcc-image useradd -u $(id -u) -g $(id -g) -m $USER
docker exec -it tpcc-image chown -R $USER /tpcc

docker exec -it --user $(id -u):$(id -g) tpcc-image sh -c "/tpcc/tpcc/init.sh"
