#!/usr/bin/env bash

if [[ $EUID == 0 ]]; then
   echo "This script must be run as non-root user inside docker group"
   exit 1
fi

docker-compose -f docker-compose.yml up -d
docker exec -it tpcc-image groupadd -g $(id -g) grp
docker exec -it tpcc-image useradd -u $(id -u) -g $(id -g) -m $USER
docker exec -it tpcc-image chown -R $USER /tpcc
